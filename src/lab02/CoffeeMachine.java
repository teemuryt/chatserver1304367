/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab02;

/**
 *
 * @author Teemu
 */
public class CoffeeMachine {
    private int waterLevel; //unit is ml
    private int beans; //unit is grams
    private int coffeecount; //counts coffees, leads to clean
    private int rinse; //unit is ml
    private int clean; //uses water(ml), happens when coffeecount reaches X, after clean reset coffeecount
    private boolean powerOn;
    private int espressowater;
    private int espressobeans;
    private int coffeewater;
    private int coffeebeans;
    private int maxwaterLevel;
    private int maxbeans;
    
    public CoffeeMachine() {
        this.waterLevel = 50;
        this.beans = 0;
        this.coffeecount = 0;
        this.powerOn = false;
        this.espressowater = 30; //ml
        this.espressobeans = 8; // g
        this.coffeewater = 150; //ml 
        this.coffeebeans = 10; //g
        this.rinse = 25; //ml
        this.clean = 300; //ml
        this.maxwaterLevel = 1000; //ml
        this.maxbeans = 100;
    }
    public void powerOnOff() {
        if (this.powerOn){
            if (this.waterLevel >= 25){
            System.out.println("Rinsing, please wait.");
            this.waterLevel = this.waterLevel - rinse;
            System.out.println("Rinsing finished.\nCoffeemachine off.\n");
            this.powerOn = false;
            }
            else {
                System.out.println("Not enough water, please fill watertank.\n");
            }
        }
        else {
            if (this.waterLevel >= 25) {
            this.powerOn = true;
            System.out.println("Coffeemachine on.\nRinsing, please wait.");
            this.waterLevel = this.waterLevel - rinse;
            System.out.println("Rinsing finished.\n");
        }
            else {
                System.out.println("Not enough water, please fill watertank.\n");
            }
                
            }
    }
    public void coffeebutton (){
        if (powerOn){
            if (coffeecount <= 10){
            if (this.waterLevel >= 150){
                System.out.println("Making coffee, please wait.");
                this.waterLevel = this.waterLevel - coffeewater;
                this.beans = this.beans - coffeebeans;
                System.out.println("Enjoy your coffee.\n"); 
                this.coffeecount ++;
            }
              
            else{ System.out.println("Not enough water, please fill watertank.\n");
            }
            }
            else {
                System.out.println("Too many drinks have been made, please clean the coffeemachine.\n");
            }
            
        }
        else {}
    }
    public void espressobutton (){
        if (powerOn){
            if (coffeecount <= 10){
            if (this.waterLevel >= 30){
                System.out.println("Making espresso, please wait.");
                this.waterLevel = this.waterLevel - espressowater;
                this.beans = this.beans - espressobeans;
                System.out.println("Enjoy your espresso.\n");
                this.coffeecount ++;
            }
            else {
                System.out.println("Not enough water, please fill watertank.\n");
            }
            }
            else {
                System.out.println("Too many drinks have been made, please clean the coffeemachine.\n");
            }
        }
         else {
    }

    
}
    public void cleanbutton () {
        if (powerOn) {
            if (this.waterLevel >= 300) {
                System.out.println("Cleaning coffeemachine, please wait.");
               this.waterLevel = this.waterLevel - clean;
                System.out.println("Coffeemachine has been cleaned.\n");
            }
            else {
                System.out.println("Not enough water, please fill watertank.\n");
            }
        }
        else {}
        
    }
    public void fillwatertank () {
        System.out.println("Water tank is being filled.");
        this.waterLevel = this.maxwaterLevel;
        System.out.println("Water tank is now full.\n");
    }
    public void fillbeantank () {
        System.out.println("Bean tank is being filled.");
        this.beans = this.maxbeans;
        System.out.println("Bean tank is now full.\n");
    }
    public void printlevels(){
        System.out.println("Current waterlevel: " + waterLevel + "ml");
        System.out.println("Current beanlevel: " + beans + "g");
    }
}

  
