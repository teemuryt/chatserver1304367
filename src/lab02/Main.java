/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab02;

import java.util.Scanner;

/**
 *
 * @author Teemu
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        CoffeeMachine coffeemachine1 = new CoffeeMachine();
        while(true){
            System.out.println("This is a coffee machine");
            coffeemachine1.printlevels();
        System.out.println("Choose from the following options:");
        System.out.println("1. On/Off");
        System.out.println("2. Fill water tank");
        System.out.println("3. Fill bean tank");
        System.out.println("4. Coffee");
            System.out.println("5. Espresso");
            System.out.println("6. Clean");
        int choice = Integer.parseInt(reader.nextLine());
        if (choice == 1){
            coffeemachine1.powerOnOff();
        }
        else if (choice == 2){
            coffeemachine1.fillwatertank();
        }
        else if (choice == 3){
            coffeemachine1.fillbeantank();
        }
        else if (choice ==4 ){
            coffeemachine1.coffeebutton();
        }
        else if (choice == 5){
            coffeemachine1.espressobutton();
        }
        else if (choice == 6){
            coffeemachine1.cleanbutton();
        }
        else {
            System.out.println("You have to choose between options 1-4.");
        }
// TODO code application logic here
    }
    }
    
    
}
